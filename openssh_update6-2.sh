#/bin/bash
# backup ssh config
mkdir /root/ssh_bak
\cp -r /etc/ssh /root/ssh_bak/
\cp -r /root/.ssh /root/ssh_bak/
# unzip openssh package and install
tar xf openssh-9.5p1-1.el6-x86_64.tgz -C /root
rpm -ivh /root/ssh/*.rpm --nodeps --force
# unzip openssl and install
yum install -y libXt-devel imake gtk2-devel gcc krb5-devel openssl-devel pam-devel
cd /root/ssh
tar xf openssl-1.1.1w.tar.gz -C /root/
cd /root/openssl-1.1.1w
./config --openssldir=/usr/local/openssl && make && make install
# config openssl
ln -sf /usr/local/openssl/include/openssl /usr/include
ln -sf /root/openssl-1.1.1w/libcrypto.so /usr/local/lib/libcrypto.so
echo 'export PATH=/usr/local/openssl/bin:$PATH' >/etc/profile
echo '/usr/local/include/openssl/' >> /etc/ld.so.conf
echo '/usr/local/lib64' >> /etc/ld.so.conf
ldconfig -v
source /etc/profile
openssl version
# config openssh and restart sshd service
echo 'UsePAM yes' >> /etc/ssh/sshd_config
\cp /root/ssh/sshd /etc/pam.d/sshd
/etc/init.d/sshd restart
