#!/bin/bash
mkdir /root/ssh_bak
\cp -r /etc/ssh /root/ssh_bak/
\cp -r /root/.ssh /root/ssh_bak/
tar xf openssh-9.5p1-1.el8.x86_64.tgz
yum install -y ssh/*.rpm
rm -f /etc/ssh/ssh_host_*
sed -ir '/ssh_host_rsa_key.pub/d' /etc/rc.d/init.d/sshd
sed -ir '/ssh_host_ecdsa_key.pub/d' /etc/rc.d/init.d/sshd
\cp ssh/sshd /etc/pam.d/sshd
sed -ir 's/#PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -ir 's/#UsePAM.*/UsePAM yes/g' /etc/ssh/sshd_config
systemctl daemon-reload
systemctl restart sshd
rpm -qa openssh|grep 9.5
[ $? -eq 0 ] && echo -en "\e[5;32mUpdate Successful\e[0m\n"
ssh -V
