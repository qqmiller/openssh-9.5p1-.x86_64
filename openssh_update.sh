#!/bin/bash

if [ `egrep  -v '^#|^$' /etc/ssh/sshd_config|wc -l` -gt 19 ] ;then
    echo -en "\e[5;31mPlease check sshd_config,save and snap the vm!\e[0m\n"
    exit 3
fi
version(){
    grep ' 8' /etc/redhat-release
    [ $? -eq 0 ] && bash openssh_update8.sh
    grep ' 7' /etc/redhat-release
    [ $? -eq 0 ] && bash openssh_update7.sh
    grep ' 6' /etc/redhat-release
    [ $? -eq 0 ] && bash openssh_update6.sh
    grep ' 5' /etc/redhat-release
    [ $? -eq 0 ] && echo "release 5 can not update to openssh9.5"
}
version
