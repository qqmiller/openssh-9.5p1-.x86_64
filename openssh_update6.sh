#!/bin/bash
mkdir /root/ssh_bak
\cp -r /etc/ssh /root/ssh_bak/
\cp -r /root/.ssh /root/ssh_bak/
tar xf openssh-9.4p1-1.el6-x86_64.tgz
yum install -y ssh/*.rpm
/etc/init.d/sshd restart
rpm -qa openssh|grep 9.4
[ $? -eq 0 ] && echo -en "\e[5;32mUpdate Successful\e[0m\n"
ssh -V
