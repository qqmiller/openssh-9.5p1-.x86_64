#!/bin/bash
init(){
    [ -d /root/ssh ] && rm -rf /root/ssh
    [ -d /root/ssh_bak ] || mkdir /root/ssh_bak
}
backup(){
    \cp /etc/ssh/* /root/ssh_bak/
    [ -d /root/.ssh ] && \cp -r  /root/.ssh ssh_bak/
}


install(){
    tar xf openssh-9.5p1-1.el7.x86_64.tgz
    yum install -y ssh/*.rpm
    \cp /etc/ssh/sshd_config.rpmnew /etc/ssh/sshd_config
    sed -ir 's/#PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config
    rm -f  /etc/ssh/ssh_host_*
    ssh-keygen -A
    systemctl restart sshd
    systemctl status sshd
}

init
backup
install
rpm -qa openssh|grep 9.5
[ $? -eq 0 ] && echo -en "\e[5;32mUpdate Successful\e[0m\n"
ssh -V
